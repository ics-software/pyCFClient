'''
Created on Apr 4, 2011

@author: shroffk
'''

from setuptools import setup

from os import path

cur_dir = path.abspath(path.dirname(__file__))

extras_require = {
    'PySide': ['PySide'],
    'pyepics': ['pyepics'],
    'perf': ['psutil'],
    'testing-ioc': ['pcaspy'],
    'test': ['codecov', 'pytest', 'pytest-cov', 'coverage', 'coveralls', 'pcaspy']
}

setup(
    name='channelfinder',
    version='3.0.0.post2',
    description='Python ChannelFinder Client Lib',
    author='Kunal Shroff',
    author_email='shroffk@bnl.gov',
    url='http://channelfinder.sourceforge.net/channelfinderpy',
    scripts=['cf-update-ioc', 'cf-monitor-test'],
    packages=['channelfinder', 'channelfinder/util', 'channelfinder/cfUpdate', 'test'],
    long_description="""\
    Python ChannelFinder Client Lib
    """,
    license='GPL',
    install_requires=['requests>=2.15.0', 'simplejson>=3.10.0', 'urllib3>=1.22'],
)
